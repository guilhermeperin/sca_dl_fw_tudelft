from flask import Flask, render_template
from commons.sca_database import ScaDatabase
from commons.sca_views import ScaViews
from commons.sca_views_pkc import ScaViewsPKC
from database.tables import KeyRankJSON, SuccessRateJSON, AccuracyIterationJSON
from database.tables import Analysis, NeuralNetwork, LeakageModel, HyperParameter
import os
import time
from datetime import datetime
import flaskcode

app = Flask(__name__,
            static_url_path='',
            static_folder='webapp/static',
            template_folder='webapp/templates')

app.jinja_env.auto_reload = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config.from_object(flaskcode.default_config)
app.config['FLASKCODE_RESOURCE_BASEPATH'] = 'scripts_aes'
app.register_blueprint(flaskcode.blueprint, url_prefix='/scripts_aes')


@app.before_request
def before_request():
    app.jinja_env.cache = {}


app.before_request(before_request)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/dashboard')
def dashboard():
    return render_template("dashboard/index.html")


@app.route('/scripts_aes')
def scripts_aes():
    return "ok"


@app.route('/tables')
def table():
    if os.path.exists("scripts_aes/database.sqlite"):

        db = ScaDatabase('scripts_aes/database.sqlite')
        analysis_all = db.select_all(Analysis)
        analyses = []

        for analysis in analysis_all:
            analysis_datetime = datetime.strptime(str(analysis.datetime), "%Y-%m-%d %H:%M:%S.%f").__format__(
                "%b %d, %Y %H:%M:%S")

            final_key_ranks = db.select_final_key_rank_json_from_analysis(KeyRankJSON, analysis.id)
            final_success_rates = db.select_final_success_rate_from_analysis(SuccessRateJSON, analysis.id)
            neural_network = db.select_from_analysis(NeuralNetwork, analysis.id)
            hyper_parameters = db.select_from_analysis(HyperParameter, analysis.id)
            analyses.append({
                "id": analysis.id,
                "script": analysis.script,
                "datetime": analysis_datetime,
                "dataset": analysis.dataset,
                "elapsed_time": time.strftime('%H:%M:%S', time.gmtime(analysis.elapsed_time)),
                "key_ranks": final_key_ranks,
                "success_rates": final_success_rates,
                "neural_network": neural_network.description,
                "neural_network_name": neural_network.model_name,
                "hyper_parameters": hyper_parameters.hyper_parameters
            })

        return render_template("tables.html", analyses=analyses)
    return render_template("tables.html", analyses=[])


@app.route('/tables_pkc')
def table_pkc():
    if os.path.exists("scripts_pkc/database.sqlite"):

        db = ScaDatabase('scripts_pkc/database.sqlite')
        analysis_all = db.select_all(Analysis)
        analyses = []

        for analysis in analysis_all:
            analysis_datetime = datetime.strptime(str(analysis.datetime), "%Y-%m-%d %H:%M:%S.%f").__format__(
                "%b %d, %Y %H:%M:%S")
            max_accuracy = db.select_max_pkc_accuracy_from_analysis(AccuracyIterationJSON, "max_trace_accuracy", analysis.id)
            if max_accuracy is not None:
                max_acc = max_accuracy['max']
            else:
                max_acc = 0
            neural_network = db.select_from_analysis(NeuralNetwork, analysis.id)
            hyper_parameters = db.select_from_analysis(HyperParameter, analysis.id)
            analyses.append({
                "id": analysis.id,
                "script": analysis.script,
                "datetime": analysis_datetime,
                "dataset": analysis.dataset,
                "elapsed_time": time.strftime('%H:%M:%S', time.gmtime(analysis.elapsed_time)),
                "max_accuracy": max_acc,
                "neural_network": neural_network.description,
                "neural_network_name": neural_network.model_name,
                "hyper_parameters": hyper_parameters.hyper_parameters
            })

        return render_template("tables_pkc.html", analyses=analyses)
    return render_template("tables_pkc.html", analyses=[])


@app.route('/script/<int:analysis_id>')
def script(analysis_id):
    db = ScaDatabase('scripts_aes/database.sqlite')

    analysis = db.select_analysis(Analysis, analysis_id)

    # get model information
    file_contents = ""

    dir_name = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file_path = os.path.join(dir_name, 'deeplearning_sca_webapp\\scripts_aes')
    filename = file_path + "\\" + analysis.script

    file = open(filename, 'r')
    lines = file.readlines()
    for line in lines:
        file_contents += line

    return render_template("dashboard/script.html", file_contents=file_contents, script_file_name=analysis.script)


@app.route('/result/<int:analysis_id>')
def result(analysis_id):
    db = ScaDatabase('scripts_aes/database.sqlite')

    sca_views = ScaViews(analysis_id)

    all_metric_plots = sca_views.metric_plots()
    all_key_rank_plots = sca_views.key_rank_plots()
    all_success_rate_plots = sca_views.success_rate_plots()

    # get neural network information from database
    analysis = db.select_analysis(Analysis, analysis_id)

    # get neural network information from database
    neural_network_model = db.select_from_analysis(NeuralNetwork, analysis_id)

    # get training hyper-parameters information from database
    hyper_parameters = db.select_from_analysis(HyperParameter, analysis_id)
    training_hyper_parameters = hyper_parameters.hyper_parameters

    # get leakage model information from database
    leakage_models = db.select_from_analysis(LeakageModel, analysis_id)
    leakage_model_parameters = leakage_models.leakage_model

    return render_template("dashboard/result.html",
                           all_plots=all_metric_plots,
                           all_key_rank_plots=all_key_rank_plots,
                           all_success_rate_plots=all_success_rate_plots,
                           neural_network_description=neural_network_model.description,
                           training_hyper_parameters=training_hyper_parameters,
                           leakage_model_parameters=leakage_model_parameters,
                           analysis=analysis)


@app.route('/result_pkc/<int:analysis_id>')
def result_pkc(analysis_id):
    db = ScaDatabase('scripts_pkc/database.sqlite')

    sca_views_pkc = ScaViewsPKC(analysis_id)

    all_accuracy_plots = sca_views_pkc.accuracy_pkc_plots()
    all_accuracy_iteration_plots = sca_views_pkc.accuracy_iteration_pkc_plots()

    # get neural network information from database
    analysis = db.select_analysis(Analysis, analysis_id)

    # get neural network information from database
    neural_network_model = db.select_from_analysis(NeuralNetwork, analysis_id)

    # get training hyper-parameters information from database
    hyper_parameters = db.select_from_analysis(HyperParameter, analysis_id)
    training_hyper_parameters = hyper_parameters.hyper_parameters

    return render_template("dashboard/result_pkc.html",
                           all_accuracy_plots=all_accuracy_plots,
                           all_accuracy_iteration_plots=all_accuracy_iteration_plots,
                           neural_network_description=neural_network_model.description,
                           training_hyper_parameters=training_hyper_parameters,
                           analysis=analysis)


if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port='5001')
