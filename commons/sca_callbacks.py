from keras.callbacks import Callback
import numpy as np


class TestCallback(Callback):
    def __init__(self, x_data, y_data, save_model=False):
        self.current_epoch = 0
        self.x = x_data
        self.y = y_data
        self.accuracy = []
        self.recall = []
        self.loss = []
        self.save_model = save_model

    def on_epoch_end(self, epoch, logs={}):
        self.accuracy.append(logs.get('val_acc'))
        self.recall.append(logs.get('val_recall'))
        self.loss.append(logs.get('val_loss'))

    def get_accuracy(self):
        return self.accuracy

    def get_recall(self):
        return self.recall

    def get_loss(self):
        return self.loss


class CalculateAccuracyCurve25519(Callback):
    def __init__(self, x_data, y_true, y_ha, number_of_epochs):
        self.x_data = x_data
        self.labels_true = y_true
        self.labels_ha = y_ha
        self.max_accuracy = 0
        self.max_accuracy_per_epoch = np.zeros(number_of_epochs)
        self.nbits_per_trace = 255
        self.number_of_validation_traces = int(len(self.labels_true) / self.nbits_per_trace)
        self.correct_bits_dl = np.zeros((self.number_of_validation_traces, number_of_epochs))
        self.correct_bits_ha = np.zeros((self.number_of_validation_traces, number_of_epochs))

    def on_epoch_end(self, epoch, logs=None):

        output_probabilities = self.model.predict(self.x_data)

        for trace_index in range(self.number_of_validation_traces):
            trace_probabilities = output_probabilities[
                                  trace_index * self.nbits_per_trace:(trace_index + 1) * self.nbits_per_trace]
            trace_labels_true = self.labels_true[
                                trace_index * self.nbits_per_trace:(trace_index + 1) * self.nbits_per_trace]
            trace_labels_ha = self.labels_ha[
                              trace_index * self.nbits_per_trace:(trace_index + 1) * self.nbits_per_trace]

            for bit_index in range(len(trace_probabilities)):
                if trace_probabilities[bit_index][1] > 0.5 and trace_labels_true[bit_index] == 1:
                    self.correct_bits_dl[trace_index][epoch] += 1
                if trace_probabilities[bit_index][0] > 0.5 and trace_labels_true[bit_index] == 0:
                    self.correct_bits_dl[trace_index][epoch] += 1
                if trace_labels_true[bit_index] == trace_labels_ha[bit_index]:
                    self.correct_bits_ha[trace_index][epoch] += 1
            print("Correct rate trace " + str(trace_index) + " :" + str(
                self.correct_bits_dl[trace_index][epoch] / self.nbits_per_trace) + " / ha: " + str(
                self.correct_bits_ha[trace_index][epoch] / self.nbits_per_trace))

            self.correct_bits_dl[trace_index][epoch] /= self.nbits_per_trace
            self.correct_bits_ha[trace_index][epoch] /= self.nbits_per_trace

            if self.correct_bits_dl[trace_index][epoch] > self.max_accuracy:
                self.max_accuracy = self.correct_bits_dl[trace_index][epoch]

            self.max_accuracy_per_epoch[epoch] = self.max_accuracy

        print("\nMax Accuracy: " + str(self.max_accuracy))

    def get_correct_dl(self):
        return self.correct_bits_dl

    def get_correct_ha(self):
        return self.correct_bits_ha

    def get_max_accuracy(self):
        return self.max_accuracy

    def get_max_accuracy_per_epoch(self):
        return self.max_accuracy_per_epoch
