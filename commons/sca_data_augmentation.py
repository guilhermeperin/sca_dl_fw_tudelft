import random
import numpy as np


class ScaDataAugmentation:

    def __init__(self):
        self.std = None
        self.mean = None
        # self.noise = None

    def initialize_parameters(self, x, param):
        samples = np.zeros((param["number_of_samples"], len(x)))
        self.std = np.zeros(param["number_of_samples"])
        for j in range(param["number_of_samples"]):
            for i in range(len(x)):
                samples[j][i] = x[i][j]
            self.std[j] = np.std(samples[j])

        # self.noise = np.zeros(param["number_of_samples"])
        # for sample in range(param["number_of_samples"]):
        #     self.noise[sample] = np.random.normal(0, self.std[sample], 1)

    # ---------------------------------------------------------------------------------------------------------------------#
    #  Functions for Data Augmentation
    # ---------------------------------------------------------------------------------------------------------------------#
    def data_augmentation_shifts(self, data_set_samples, data_set_labels, param, mlp=False):
        ns = param["number_of_samples"]

        while True:

            x_train_shifted = np.zeros((param["mini-batch"], ns))
            rnd = random.randint(0, len(data_set_samples) - param["mini-batch"])
            x_mini_batch = data_set_samples[rnd:rnd + param["mini-batch"]]

            for trace_index in range(param["mini-batch"]):
                x_train_shifted[trace_index] = x_mini_batch[trace_index]
                shift = random.randint(-5, 5)
                if shift > 0:
                    x_train_shifted[trace_index][0:ns - shift] = x_mini_batch[trace_index][shift:ns]
                    x_train_shifted[trace_index][ns - shift:ns] = x_mini_batch[trace_index][0:shift]
                else:
                    x_train_shifted[trace_index][0:abs(shift)] = x_mini_batch[trace_index][ns - abs(shift):ns]
                    x_train_shifted[trace_index][abs(shift):ns] = x_mini_batch[trace_index][0:ns - abs(shift)]

            if mlp:
                yield x_train_shifted, data_set_labels[rnd:rnd + param["mini-batch"]]
            else:
                x_train_shifted_reshaped = x_train_shifted.reshape((x_train_shifted.shape[0], x_train_shifted.shape[1], 1))
                yield x_train_shifted_reshaped, data_set_labels[rnd:rnd + param["mini-batch"]]

    def data_augmentation_noise(self, data_set_samples, data_set_labels, param, mlp=False):
        ns = param["number_of_samples"]

        while True:

            x_train_augmented = np.zeros((param["mini-batch"], ns))
            rnd = random.randint(0, len(data_set_samples) - param["mini-batch"])
            x_mini_batch = data_set_samples[rnd:rnd + param["mini-batch"]]

            # noise = np.zeros(param["number_of_samples"])
            # for sample in range(param["number_of_samples"]):
            #     noise[sample] = np.random.normal(0, self.std[sample], 1)
            noise = np.random.normal(0, 1, param["number_of_samples"])

            for trace_index in range(param["mini-batch"]):
                x_train_augmented[trace_index] = x_mini_batch[trace_index] + noise

            if not mlp:
                x_train_augmented_reshaped = x_train_augmented.reshape(
                    (x_train_augmented.shape[0], x_train_augmented.shape[1], 1))

                yield x_train_augmented_reshaped, data_set_labels[rnd:rnd + param["mini-batch"]]
            yield x_train_augmented, data_set_labels[rnd:rnd + param["mini-batch"]]
