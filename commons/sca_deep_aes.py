import numpy as np
import pandas as pd
import time
import os
import h5py
from keras import backend
from keras.utils import to_categorical
from crypto.aes import AES
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import TestCallback
from commons.sca_keras_models import ScaKerasModels
from commons.sca_functions import ScaFunctions
from database.database_inserts import DatabaseInserts


class ScaDeep:

    def __init__(self):
        self.target_trace_set = None
        self.sca_parameters = ScaParameters()
        self.target_params = None
        self.aes_lm = None
        self.crypto = AES()
        self.callbacks = []
        self.model = None
        self.random_model = None
        self.model_obj = None
        self.model_name = None
        self.db_inserts = None

        self.metric_names = None
        self.keras_model_metrics = None

        self.ge_test = None
        self.sr_test = None
        self.metric_training = None
        self.metric_validation = None

        self.hyper_parameters = []
        self.learning_rate = None
        self.optimizer = None

        self.mlp = None
        self.cnn = None

        self.ge_runs = 1
        self.sr_runs = 1
        self.target_key_bytes = None
        self.key_int = None
        self.first_key_byte = True

        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    def target(self, target):
        self.target_trace_set = target
        self.sca_parameters = ScaParameters()
        self.target_params = self.sca_parameters.get_trace_set(self.target_trace_set)

    def sca_parameters(self):
        return self.sca_parameters

    def aes_leakage_model(self, leakage_model="HW", bit=0, byte=0):
        self.aes_lm = {
            "leakage_model": leakage_model,
            "bit": bit,
            "byte": byte,
        }

        if self.target_params is not None:
            if self.aes_lm["leakage_model"] == "HW":
                self.target_params["classes"] = 9
            elif self.aes_lm["leakage_model"] == "ID":
                self.target_params["classes"] = 256
            else:
                self.target_params["classes"] = 2
        else:
            print("Parameters (param) from target is not selected. Set target before the leakage model.")

        return self.aes_lm

    def train_validation_sets(self):
        hf = h5py.File('../datasets/' + self.target_trace_set + '.h5', 'r')
        train_samples = np.array(hf.get('profiling_traces'))
        train_data = np.array(hf.get('profiling_data'))

        training_dataset_reshaped = train_samples.reshape((train_samples.shape[0], train_samples.shape[1], 1))

        x_train = training_dataset_reshaped[0:self.target_params["n_train"]]
        x_val = training_dataset_reshaped[
                self.target_params["n_train"]:self.target_params["n_train"] + self.target_params["n_validation"]]
        y_train = train_data[0:self.target_params["n_train"]]
        y_validation = train_data[
                       self.target_params["n_train"]:self.target_params["n_train"] + self.target_params["n_validation"]]
        val_samples = train_samples[
                      self.target_params["n_train"]:self.target_params["n_train"] + self.target_params["n_validation"]]

        return x_train, x_val, train_samples[0:self.target_params["n_train"]], val_samples, y_train, y_validation

    def test_set(self):
        hf = h5py.File('../datasets/' + self.target_trace_set + '.h5', 'r')
        test_samples = np.array(hf.get('attacking_traces'))
        test_data = np.array(hf.get('attacking_data'))

        test_samples = test_samples[0:self.target_params["n_test"]]
        y_test = test_data[0:self.target_params["n_test"]]

        x_test = test_samples.reshape((test_samples.shape[0], test_samples.shape[1], 1))

        return x_test, test_samples, y_test

    def add_callback(self, callback):
        self.callbacks.append(callback)
        return self.callbacks

    def keras_model(self, model, mlp=False, cnn=False):
        self.model_name = model
        self.model_obj = model
        self.model = model(self.target_params["classes"], self.target_params["number_of_samples"])
        self.mlp = mlp
        self.cnn = cnn

    def get_model(self):
        return self.model

    def get_test_guessing_entropy(self):
        return self.ge_test

    def get_test_success_rate(self):
        return self.sr_test

    def __set_metrics(self):
        self.metric_names = self.model.metrics_names
        self.keras_model_metrics = len(self.metric_names)

    def initialize_metric_training_validation(self):
        self.metric_training = np.zeros((self.keras_model_metrics, self.target_params["epochs"]))
        self.metric_validation = np.zeros((self.keras_model_metrics, self.target_params["epochs"]))

    def initialize_result_vectors(self, nt_test, nt_kr):
        self.ge_test = np.zeros(nt_kr)
        self.sr_test = np.zeros(nt_test)

    def initialize_configurations(self, target_key_bytes, ge_sr):

        if ge_sr is not None:
            self.ge_runs = ge_sr[0]

        self.key_int = ([int(x) for x in bytearray.fromhex(self.target_params["key"])])
        if target_key_bytes is None:
            self.target_key_bytes = list(range(0, len(self.key_int)))

        self.first_key_byte = True

    def run(self, target_key_bytes=None, ge_sr=None):

        self.__set_metrics()
        self.hyper_parameters = []
        self.initialize_configurations(target_key_bytes, ge_sr)

        x_train, x_val, train_samples, val_samples, train_data, validation_data = self.train_validation_sets()
        x_test, test_samples, test_data, = self.test_set()

        x_t = train_samples if self.mlp else x_train
        x_v = val_samples if self.mlp else x_val
        x_t1 = test_samples if self.mlp else x_test

        report_ge_sr_interval = ge_sr[1]
        fraction_ge_sr_set = ge_sr[2]
        nt_test = len(test_samples)
        nt_kr = int(nt_test / (report_ge_sr_interval * fraction_ge_sr_set))

        self.initialize_metric_training_validation()
        self.initialize_result_vectors(nt_test, nt_kr)

        start = time.time()

        for key_byte_index in target_key_bytes:
            self.target_params["good_key"] = self.key_int[key_byte_index]
            self.aes_lm["byte"] = key_byte_index

            train_labels = self.crypto.aes_labelize(train_data, self.aes_lm, self.target_params)
            validation_labels = self.crypto.aes_labelize(validation_data, self.aes_lm, self.target_params)
            test_labels = self.crypto.aes_labelize(test_data, self.aes_lm, self.target_params)

            y_train = to_categorical(train_labels, num_classes=self.target_params["classes"])
            y_val = to_categorical(validation_labels, num_classes=self.target_params["classes"])
            y_test = to_categorical(test_labels, num_classes=self.target_params["classes"])

            callback_test = TestCallback(x_t1, y_test)

            print("\nKey Byte {}\n".format(key_byte_index))

            os.environ["CUDA_VISIBLE_DEVICES"] = "0"

            history = self.model.fit(
                x=x_t,
                y=y_train,
                batch_size=self.target_params["mini-batch"],
                verbose=1,
                epochs=self.target_params["epochs"],
                shuffle=True,
                validation_data=(x_v, y_val),
                callbacks=[callback_test])

            os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

            model_name = self.model_obj.__name__
            self.learning_rate = backend.eval(self.model.optimizer.lr)
            self.optimizer = self.model.optimizer.__class__.__name__

            self.compute_ge_and_sr(x_t1, test_data, report_ge_sr_interval, fraction_ge_sr_set)
            self.set_hyper_parameters(nt_test, report_ge_sr_interval, fraction_ge_sr_set)
            self.get_metrics_results(history)
            self.save_results_in_database(time.time() - start, model_name)
            self.save_metrics(callback_test.get_accuracy())
            self.save_results()

            backend.clear_session()

    def __save_metric_avg(self, metric, n_models, name):
        kr_avg = sum(metric[n] for n in range(n_models)) / n_models
        self.db_inserts.save_metric(kr_avg, self.aes_lm["byte"], name)

    def __save_metric(self, metric, name):
        self.db_inserts.save_metric(metric, self.aes_lm["byte"], name)

    def __save_kr(self, kr):
        self.db_inserts.save_key_rank_json(pd.Series(kr).to_json(), self.aes_lm["byte"])

    def __save_sr(self, sr):
        self.db_inserts.save_success_rate_json(pd.Series(sr).to_json(), self.aes_lm["byte"])

    def __save_final_ge(self, final_ge_test, final_ge_val):
        self.db_inserts.save_ensemble(pd.Series(final_ge_val).to_json(), self.aes_lm["byte"], "Validation")
        self.db_inserts.save_ensemble(pd.Series(final_ge_test).to_json(), self.aes_lm["byte"], "Test")

    def __set_hyper_parameters(self, key_rank):
        self.hyper_parameters.append({
            "key_rank": key_rank,
            "mini_batch": self.target_params["mini-batch"],
            "epochs": self.target_params["epochs"],
            "learning_rate": float(self.learning_rate),
            "optimizer": str(self.optimizer),
            "training_set": self.target_params["n_train"],
            "validation_set": self.target_params["n_validation"],
            "test_set": self.target_params["n_test"]
        })

    def save_results_in_database(self, elapsed_time, model_name):
        if self.first_key_byte:
            self.db_inserts = DatabaseInserts('database.sqlite', self.target_params["name"],
                                              "script_single_all_keys.py", elapsed_time)

            leakage_model = [{
                "cipher": "AES",
                "model": self.aes_lm["leakage_model"],
                "operation": "S-Box"
            }]

            sca_keras_model = ScaKerasModels()
            keras_model_description = sca_keras_model.keras_model_as_string(model_name)

            self.db_inserts.save_neural_network(keras_model_description, model_name)
            self.db_inserts.save_hyper_parameters(self.hyper_parameters)
            self.db_inserts.save_leakage_model(leakage_model)

            self.first_key_byte = False
        else:
            self.db_inserts.update_elapsed_time_analysis(elapsed_time)

    def save_metrics(self, test_acc):
        m_index = 0
        for metric in self.metric_names:
            self.__save_metric(self.metric_training[m_index], metric)
            self.__save_metric(self.metric_validation[m_index], "val_" + metric)
            if metric == "accuracy":
                self.__save_metric(test_acc, "test_" + metric)
            m_index += 1

    def save_results(self):
        self.__save_kr(self.ge_test)
        self.__save_sr(self.sr_test)

    def compute_ge_and_sr(self, x_t1, test_data_1, step, fraction):
        self.ge_test, self.sr_test = ScaFunctions().ge_and_sr(self.ge_runs, self.model, self.target_params,
                                                              self.aes_lm, x_t1, test_data_1, step, fraction)

    def set_hyper_parameters(self, nt_test, step, fraction):
        self.__set_hyper_parameters(self.ge_test[int(nt_test / (step * fraction)) - 1])

    def get_metrics_results(self, history):
        m_index = 0
        for metric in self.metric_names:
            self.metric_training[m_index] = history.history[metric]
            self.metric_validation[m_index] = history.history['val_' + metric]
            m_index += 1
