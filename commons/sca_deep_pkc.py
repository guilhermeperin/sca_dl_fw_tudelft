import numpy as np
import pandas as pd
import time
import os
import h5py
import random
from keras import backend
from keras.utils import to_categorical
from commons.sca_parameters import ScaParameters
from commons.sca_callbacks import TestCallback, CalculateAccuracyCurve25519
from commons.sca_keras_models import ScaKerasModels
from commons.sca_data_augmentation import ScaDataAugmentation
from database.database_inserts import DatabaseInserts
from sklearn.utils import shuffle
from sklearn.cluster import KMeans
from scipy import stats
import matplotlib.pyplot as plt


class ScaDeepPKC:

    def __init__(self):
        self.target_trace_set = None
        self.sca_parameters = ScaParameters()
        self.target_params = None
        self.callbacks = []
        self.model = None
        self.model_obj = None
        self.model_name = None
        self.db_inserts = None
        self.metric_names = None
        self.keras_model_metrics = None
        self.metric_training = None
        self.metric_validation = None
        self.hyper_parameters = []
        self.learning_rate = None
        self.optimizer = None
        self.mlp = None
        self.cnn = None
        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    def target(self, target):
        self.target_trace_set = target
        self.sca_parameters = ScaParameters()
        self.target_params = self.sca_parameters.get_trace_set(self.target_trace_set)

    def sca_parameters(self):
        return self.sca_parameters

    def set_mini_batch(self, mini_batch):
        self.target_params["mini-batch"] = mini_batch

    def set_epochs(self, epochs):
        self.target_params["epochs"] = epochs

    def train_validation_sets(self):
        hf = h5py.File('../datasets/' + self.target_trace_set + '.h5', 'r')
        train_samples = np.array(hf.get('profiling_traces'))
        train_data = np.array(hf.get('profiling_data'))

        fs = self.target_params["first_sample"]
        ns = self.target_params["number_of_samples"]
        train_samples = train_samples[:, fs: fs + ns]

        training_dataset_reshaped = train_samples.reshape((train_samples.shape[0], train_samples.shape[1], 1))

        x_train = training_dataset_reshaped[0:self.target_params["n_train"]]
        x_val = training_dataset_reshaped[
                self.target_params["n_train"]:self.target_params["n_train"] + self.target_params["n_validation"]]
        y_train = train_data[0:self.target_params["n_train"]]
        y_validation = train_data[
                       self.target_params["n_train"]:self.target_params["n_train"] + self.target_params["n_validation"]]
        val_samples = train_samples[
                      self.target_params["n_train"]:self.target_params["n_train"] + self.target_params["n_validation"]]

        return x_train, x_val, train_samples[0:self.target_params["n_train"]], val_samples, y_train, y_validation

    def test_set(self):
        hf = h5py.File('../datasets/' + self.target_trace_set + '.h5', 'r')
        test_samples = np.array(hf.get('attacking_traces'))
        test_data = np.array(hf.get('attacking_data'))

        fs = self.target_params["first_sample"]
        ns = self.target_params["number_of_samples"]
        test_samples = test_samples[:, fs: fs + ns]

        test_samples = test_samples[0:self.target_params["n_test"]]
        y_test = test_data[0:self.target_params["n_test"]]

        x_test = test_samples.reshape((test_samples.shape[0], test_samples.shape[1], 1))

        return x_test, test_samples, y_test

    def add_callback(self, callback):
        self.callbacks.append(callback)
        return self.callbacks

    def keras_model(self, model, mlp=False, cnn=False):
        self.model_name = model
        self.model_obj = model
        self.model = model(self.target_params["classes"], self.target_params["number_of_samples"])
        self.mlp = mlp
        self.cnn = cnn

    def get_model(self):
        return self.model

    def __set_metrics(self):
        self.metric_names = self.model.metrics_names
        self.keras_model_metrics = len(self.metric_names)

    def initialize_metric_training_validation(self):
        self.metric_training = np.zeros((self.keras_model_metrics, self.target_params["epochs"]))
        self.metric_validation = np.zeros((self.keras_model_metrics, self.target_params["epochs"]))

    def run(self):

        self.__set_metrics()
        self.hyper_parameters = []

        x_train, x_val, train_samples, val_samples, train_data, validation_data = self.train_validation_sets()
        x_test, test_samples, test_data, = self.test_set()

        x_t = train_samples if self.mlp else x_train
        x_v = val_samples if self.mlp else x_val
        x_t1 = test_samples if self.mlp else x_test

        self.initialize_metric_training_validation()

        start = time.time()

        labels_from_pkc_data_train_ha = [0 if row[0] == 0 else 1 for row in train_data]
        labels_from_pkc_data_train_true = [0 if row[1] == 0 else 1 for row in train_data]
        labels_from_pkc_data_validation_ha = [0 if row[0] == 0 else 1 for row in validation_data]
        labels_from_pkc_data_validation_true = [0 if row[1] == 0 else 1 for row in validation_data]
        labels_from_pkc_data_test_ha = [0 if row[0] == 0 else 1 for row in test_data]
        labels_from_pkc_data_test_true = [0 if row[1] == 0 else 1 for row in test_data]

        y_train = to_categorical(labels_from_pkc_data_train_ha, num_classes=self.target_params["classes"])
        y_val = to_categorical(labels_from_pkc_data_validation_true, num_classes=self.target_params["classes"])

        correct_bits_training_set = 0
        for index in range(len(labels_from_pkc_data_train_true)):
            if labels_from_pkc_data_train_true[index] == labels_from_pkc_data_train_ha[index]:
                correct_bits_training_set += 1

        correct_bits_validation_set = 0
        for index in range(len(labels_from_pkc_data_validation_true)):
            if labels_from_pkc_data_validation_true[index] == labels_from_pkc_data_validation_ha[index]:
                correct_bits_validation_set += 1

        correct_bits_test_set = 0
        for index in range(len(labels_from_pkc_data_test_true)):
            if labels_from_pkc_data_test_true[index] == labels_from_pkc_data_test_ha[index]:
                correct_bits_test_set += 1

        print("Correct rate for training set: " + str(correct_bits_training_set / len(labels_from_pkc_data_train_true)))
        print("Correct rate for validation set: " + str(correct_bits_validation_set / len(labels_from_pkc_data_validation_true)))
        print("Correct rate for test set: " + str(correct_bits_test_set / len(labels_from_pkc_data_test_true)))

        callback_test = TestCallback(x_v, y_val)
        callbacks_pkc_curve25519 = CalculateAccuracyCurve25519(x_t1, labels_from_pkc_data_test_true, labels_from_pkc_data_test_ha,
                                                               self.target_params["epochs"])

        os.environ["CUDA_VISIBLE_DEVICES"] = "0"

        history = self.model.fit(
            x=x_t,
            y=y_train,
            batch_size=self.target_params["mini-batch"],
            verbose=1,
            epochs=self.target_params["epochs"],
            shuffle=True,
            validation_data=(x_v, y_val),
            callbacks=[callback_test, callbacks_pkc_curve25519])

        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

        model_name = self.model_obj.__name__
        self.learning_rate = backend.eval(self.model.optimizer.lr)
        self.optimizer = self.model.optimizer.__class__.__name__

        accuracy_dl = callbacks_pkc_curve25519.get_correct_dl()
        accuracy_ha = callbacks_pkc_curve25519.get_correct_ha()

        self.set_hyper_parameters()
        self.get_metrics_results(history)
        self.save_results_in_database(time.time() - start, model_name)

        for index in range(len(accuracy_dl)):
            self.db_inserts.save_accuracy_json(pd.Series(accuracy_dl[index]).to_json(), 0, "acc_dl_set1_" + str(index))
        self.db_inserts.save_accuracy_iteration_json(pd.Series(callbacks_pkc_curve25519.get_max_accuracy_per_epoch()).to_json(), 0,
                                                     "max_trace_accuracy")
        self.db_inserts.save_accuracy_iteration_json(pd.Series(history.history['val_acc']).to_json(), 0, "average_accuracy")

        backend.clear_session()

    def run_recursive(self, number_of_rounds, data_augmentation=None):

        self.__set_metrics()
        self.hyper_parameters = []

        x_set1, x_set2, set1_samples, set2_samples, set1_data, set2_data = self.train_validation_sets()
        x_test, test_samples, test_data, = self.test_set()

        x_t1 = set1_samples if self.mlp else x_set1
        x_t2 = set2_samples if self.mlp else x_set2
        x_test = test_samples if self.mlp else x_test

        self.initialize_metric_training_validation()

        start = time.time()

        set1_ha = [0 if row[0] == 0 else 1 for row in set1_data]
        set1_true = [0 if row[1] == 0 else 1 for row in set1_data]
        set2_ha = [0 if row[0] == 0 else 1 for row in set2_data]
        set2_true = [0 if row[1] == 0 else 1 for row in set2_data]
        test_ha = [0 if row[0] == 0 else 1 for row in test_data]
        test_true = [0 if row[1] == 0 else 1 for row in test_data]
        set1_ha_relabel = np.zeros(len(x_t1))
        set2_ha_relabel = np.zeros(len(x_t2))

        y_set1 = to_categorical(set1_ha, num_classes=self.target_params["classes"])
        y_set2 = to_categorical(set2_ha, num_classes=self.target_params["classes"])
        y_test = to_categorical(test_true, num_classes=self.target_params["classes"])

        sets_samples = np.zeros((len(x_t1) + len(x_t2), self.target_params["number_of_samples"]))
        sets_samples[0:len(x_t1)] = set1_samples
        sets_samples[len(x_t1):len(x_t1) + len(x_t2)] = set2_samples
        x_sets = sets_samples.reshape((sets_samples.shape[0], sets_samples.shape[1], 1))

        correct_bits_set1 = 0
        for index in range(len(set1_true)):
            if set1_true[index] == set1_ha[index]:
                correct_bits_set1 += 1

        correct_bits_set2 = 0
        for index in range(len(set2_true)):
            if set2_true[index] == set2_ha[index]:
                correct_bits_set2 += 1

        correct_bits_test_set = 0
        for index in range(len(test_true)):
            if test_true[index] == test_ha[index]:
                correct_bits_test_set += 1

        print("Correct rate for set 1: " + str(correct_bits_set1 / len(set1_true)))
        print("Correct rate for set 2: " + str(correct_bits_set2 / len(set2_true)))
        print("Correct rate for test set: " + str(correct_bits_test_set / len(test_true)))

        accuracy_dl_set1 = np.zeros((number_of_rounds, int(len(x_test) / 255), self.target_params["epochs"]))
        accuracy_ha_set1 = np.zeros((number_of_rounds, int(len(x_test) / 255), self.target_params["epochs"]))
        accuracy_dl_set2 = np.zeros((number_of_rounds, int(len(x_test) / 255), self.target_params["epochs"]))
        accuracy_ha_set2 = np.zeros((number_of_rounds, int(len(x_test) / 255), self.target_params["epochs"]))

        model_name = self.model_obj.__name__
        self.learning_rate = backend.eval(self.model.optimizer.lr)
        self.optimizer = self.model.optimizer.__class__.__name__

        sca_data_augmentation = None
        if data_augmentation is not None:
            sca_data_augmentation = ScaDataAugmentation()

        max_accuracy = 0

        accuracy_set1 = []
        accuracy_set2 = []
        max_trace_accuracy = []
        accuracy_set1.append(correct_bits_set1 / len(set1_true))
        accuracy_set2.append(correct_bits_set2 / len(set2_true))
        max_trace_accuracy.append(np.max([correct_bits_set1 / len(set1_true), correct_bits_set2 / len(set2_true)]))

        for round_index in range(number_of_rounds):

            print("-----------------------------------------------------------------------------------------------------------------------")
            print("Round {} - Set 1".format(round_index))
            print("-----------------------------------------------------------------------------------------------------------------------")

            callbacks_pkc_curve25519 = CalculateAccuracyCurve25519(x_test, test_true, test_ha, self.target_params["epochs"])

            os.environ["CUDA_VISIBLE_DEVICES"] = "0"

            self.model = self.model_obj(self.target_params["classes"], self.target_params["number_of_samples"])
            if data_augmentation is not None:
                self.model.fit_generator(
                    generator=sca_data_augmentation.data_augmentation_shifts(set1_samples, y_set1, self.target_params, self.mlp),
                    steps_per_epoch=data_augmentation[0],
                    epochs=self.target_params["epochs"],
                    validation_data=(x_test, y_test),
                    validation_steps=1,
                    callbacks=[callbacks_pkc_curve25519])
            else:
                self.model.fit(
                    x=x_t1,
                    y=y_set1,
                    batch_size=self.target_params["mini-batch"],
                    verbose=1,
                    epochs=self.target_params["epochs"],
                    shuffle=True,
                    validation_data=(x_test, y_test),
                    callbacks=[callbacks_pkc_curve25519])

            os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

            predictions = self.model.predict(x_t2)
            for index in range(len(x_t2)):
                set2_ha_relabel[index] = 0 if predictions[index][0] > 0.5 else 1
            # if round_index > 0:
            y_set2 = to_categorical(set2_ha_relabel, num_classes=self.target_params["classes"])

            correct_bits_set2 = 0
            for index in range(len(set2_true)):
                if set2_true[index] == set2_ha_relabel[index]:
                    correct_bits_set2 += 1
            print("Correct rate for set 2: {}".format(correct_bits_set2 / len(set2_true)))
            accuracy_set2.append(correct_bits_set2 / len(set2_true))

            accuracy_dl_set1[round_index] = callbacks_pkc_curve25519.get_correct_dl()
            accuracy_ha_set1[round_index] = callbacks_pkc_curve25519.get_correct_ha()
            max_accuracy_all_epochs = callbacks_pkc_curve25519.get_max_accuracy()

            if max_accuracy_all_epochs > max_accuracy:
                max_accuracy = max_accuracy_all_epochs

            print("\nMax Accuracy: " + str(max_accuracy))
            backend.clear_session()

            print("-----------------------------------------------------------------------------------------------------------------------")
            print("Round {} - Set 2".format(round_index))
            print("-----------------------------------------------------------------------------------------------------------------------")

            callbacks_pkc_curve25519 = CalculateAccuracyCurve25519(x_test, test_true, test_ha, self.target_params["epochs"])

            os.environ["CUDA_VISIBLE_DEVICES"] = "0"

            self.model = self.model_obj(self.target_params["classes"], self.target_params["number_of_samples"])
            if data_augmentation is not None:
                self.model.fit_generator(
                    generator=sca_data_augmentation.data_augmentation_shifts(set2_samples, y_set2, self.target_params, self.mlp),
                    steps_per_epoch=data_augmentation[0],
                    epochs=self.target_params["epochs"],
                    validation_data=(x_test, y_test),
                    validation_steps=1,
                    callbacks=[callbacks_pkc_curve25519])
            else:
                self.model.fit(
                    x=x_t2,
                    y=y_set2,
                    batch_size=self.target_params["mini-batch"],
                    verbose=1,
                    epochs=self.target_params["epochs"],
                    shuffle=True,
                    validation_data=(x_test, y_test),
                    callbacks=[callbacks_pkc_curve25519])

            os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

            predictions = self.model.predict(x_t1)
            for index in range(len(x_t1)):
                set1_ha_relabel[index] = 0 if predictions[index][0] > 0.5 else 1
            if round_index > 0:
                y_set1 = to_categorical(set1_ha_relabel, num_classes=self.target_params["classes"])

            correct_bits_set1 = 0
            for index in range(len(set1_true)):
                if set1_true[index] == set1_ha_relabel[index]:
                    correct_bits_set1 += 1
            print("Correct rate for set 1: {}".format(correct_bits_set1 / len(set1_true)))
            accuracy_set1.append(correct_bits_set1 / len(set1_true))

            accuracy_dl_set2[round_index] = callbacks_pkc_curve25519.get_correct_dl()
            accuracy_ha_set2[round_index] = callbacks_pkc_curve25519.get_correct_ha()
            max_accuracy_all_epochs = callbacks_pkc_curve25519.get_max_accuracy()

            if max_accuracy_all_epochs > max_accuracy:
                max_accuracy = max_accuracy_all_epochs

            print("\nMax Accuracy: " + str(max_accuracy))
            backend.clear_session()

            max_trace_accuracy.append(max_accuracy)

            # shuffle set1 and set2
            data_relabel = np.zeros(len(x_t1) + len(x_t2))
            data_relabel[0:len(x_t1)] = set1_ha_relabel
            data_relabel[len(x_t1):len(x_t1) + len(x_t2)] = set2_ha_relabel

            data_true = np.zeros(len(x_t1) + len(x_t2))
            data_true[0:len(x_t1)] = set1_true
            data_true[len(x_t1):len(x_t1) + len(x_t2)] = set2_true

            rnd_state = random.randint(0, 100000)
            sets_samples_shuffle, relabel_shuffle, true_shuffle = shuffle(sets_samples, data_relabel, data_true, random_state=rnd_state)

            set1_ha = relabel_shuffle[0: len(x_t1)]
            set2_ha = relabel_shuffle[len(x_t1): len(x_t1) + len(x_t2)]
            set1_true = true_shuffle[0: len(x_t1)]
            set2_true = true_shuffle[len(x_t1): len(x_t1) + len(x_t2)]

            # return shuffled sets 1 and 2
            y_set1 = to_categorical(set1_ha, num_classes=self.target_params["classes"])
            y_set2 = to_categorical(set2_ha, num_classes=self.target_params["classes"])

            set1_samples = sets_samples_shuffle[0:len(x_t1)]
            set2_samples = sets_samples_shuffle[len(x_t1):len(x_t1) + len(x_t2)]

            sets_samples = sets_samples_shuffle
            x_sets = sets_samples_shuffle.reshape((sets_samples_shuffle.shape[0], sets_samples_shuffle.shape[1], 1))
            x_set1 = x_sets[0:len(x_t1)]
            x_set2 = x_sets[len(x_t1):len(x_t1) + len(x_t2)]

            x_t1 = set1_samples if self.mlp else x_set1
            x_t2 = set2_samples if self.mlp else x_set2

        #     n1 = sum(relabel_shuffle)
        #     n0 = len(x_t1) + len(x_t2) - n1
        #
        #     set1 = np.zeros((int(n1), self.target_params["number_of_samples"]))
        #     set0 = np.zeros((int(n0), self.target_params["number_of_samples"]))
        #
        #     n0_count = 0
        #     n1_count = 0
        #     for i in range(int(n0 + n1)):
        #         if relabel_shuffle[i] == 0:
        #             set0[n0_count] = sets_samples[i]
        #             n0_count += 1
        #         else:
        #             set1[n1_count] = sets_samples[i]
        #             n1_count += 1
        #
        #     t_test = stats.ttest_ind(set0, set1)[0]
        #     plt.plot(abs(t_test), label="Iteration " + str(round_index))
        # plt.legend()
        # plt.show()

        self.set_hyper_parameters()
        self.save_results_in_database(time.time() - start, model_name)

        for index in range(len(accuracy_dl_set1[number_of_rounds - 1])):
            self.db_inserts.save_accuracy_json(pd.Series(accuracy_dl_set1[number_of_rounds - 1][index]).to_json(), 0,
                                               "acc_dl_set1_" + str(index))
        for index in range(len(accuracy_dl_set2[number_of_rounds - 1])):
            self.db_inserts.save_accuracy_json(pd.Series(accuracy_dl_set2[number_of_rounds - 1][index]).to_json(), 0,
                                               "acc_dl_set2_" + str(index))

        self.db_inserts.save_accuracy_iteration_json(pd.Series(accuracy_set1).to_json(), 0, "accuracy_iteration_set1")
        self.db_inserts.save_accuracy_iteration_json(pd.Series(accuracy_set2).to_json(), 0, "accuracy_iteration_set2")
        self.db_inserts.save_accuracy_iteration_json(pd.Series(max_trace_accuracy).to_json(), 0, "max_trace_accuracy")

    def run_cluster(self):

        _, _, train_samples, _, train_data, _ = self.train_validation_sets()
        _, test_samples, test_data, = self.test_set()

        labels_from_pkc_data_train_ha = [0 if row[0] == 0 else 1 for row in train_data]
        labels_from_pkc_data_train_true = [0 if row[1] == 0 else 1 for row in train_data]
        labels_from_pkc_data_test_ha = [0 if row[0] == 0 else 1 for row in test_data]
        labels_from_pkc_data_test_true = [0 if row[1] == 0 else 1 for row in test_data]

        correct_bits_training_set = 0
        for index in range(len(labels_from_pkc_data_train_true)):
            if labels_from_pkc_data_train_true[index] == labels_from_pkc_data_train_ha[index]:
                correct_bits_training_set += 1

        correct_bits_test_set = 0
        for index in range(len(labels_from_pkc_data_test_true)):
            if labels_from_pkc_data_test_true[index] == labels_from_pkc_data_test_ha[index]:
                correct_bits_test_set += 1

        print("Correct rate for training set: " + str(correct_bits_training_set / len(labels_from_pkc_data_train_true)))
        print("Correct rate for test set: " + str(correct_bits_test_set / len(labels_from_pkc_data_test_true)))

        correct_ha = np.zeros(self.target_params["number_of_samples"])
        t_test = np.zeros((self.target_params["number_of_samples"], self.target_params["number_of_samples"]))
        t_test_max = np.zeros(self.target_params["number_of_samples"])

        plt.subplots(2, 1)
        for i in range(self.target_params["number_of_samples"]):
            samples = train_samples[:, i]
            kmeans = KMeans(n_clusters=2, random_state=0).fit(samples.reshape(-1, 1))
            kmeans_labels = kmeans.labels_

            correct_bits_cluster = 0
            for index in range(len(labels_from_pkc_data_train_true)):
                if labels_from_pkc_data_train_true[index] == kmeans_labels[index]:
                    correct_bits_cluster += 1
            correct_ha[i] = correct_bits_cluster / len(labels_from_pkc_data_train_true)
            print("correct rate for k-means on sample {}: {}".format(i, correct_ha[i]))

            # if correct_ha[i] > 0.7 or correct_ha[i] < 0.3:

            # n0_count = 0
            # n1_count = 0
            # n1 = sum(kmeans_labels)
            # n0 = len(kmeans_labels) - n1
            # set1 = np.zeros((int(n1), self.target_params["number_of_samples"]))
            # set0 = np.zeros((int(n0), self.target_params["number_of_samples"]))
            #
            # # n0 = 0
            # # n1 = 0
            # # set1 = np.zeros(self.target_params["number_of_samples"])
            # # set0 = np.zeros(self.target_params["number_of_samples"])
            #
            # for j in range(len(kmeans_labels)):
            #     if kmeans_labels[j] == 0:
            #         set0[n0_count] = train_samples[j]
            #         n0_count += 1
            #         # set0 += train_samples[j]
            #         # n0 += 1
            #     else:
            #         set1[n1_count] = train_samples[j]
            #         n1_count += 1
            #         # set1 += train_samples[j]
            #         # n1 += 1
            #
            # t_test[i] = abs(stats.ttest_ind(set0, set1)[0])
            # t_test_max[i] = max(t_test[i])
            #
            # # if n0 > 0:
            # #     set0 /= n0
            # # if n1 > 0:
            # #     set1 /= n1
            # # t_test[i] = abs(set1 - set0)
            # # t_test_max[i] = max(t_test[i])
            #
            # plt.subplot(2, 1, 1)
            # plt.plot(t_test[i])

        # print(t_test_max)
        # t_test_max_sorted = (-t_test_max).argsort()[:20]
        # print(t_test_max_sorted)
        # for k in range(20):
        #     # plt.subplot(2, 1, 2)
        #     # plt.scatter(t_test_max_sorted[k], 3500)
        #     print("correct rate for k-means on sample {}: {}".format(t_test_max_sorted[k], correct_ha[t_test_max_sorted[k]]))
        plt.subplot(2, 1, 2)
        plt.plot(correct_ha)

        plt.show()

    def __save_metric_avg(self, metric, n_models, name):
        kr_avg = sum(metric[n] for n in range(n_models)) / n_models
        self.db_inserts.save_metric(kr_avg, 0, name)

    def __save_metric(self, metric, name):
        self.db_inserts.save_metric(metric, 0, name)

    def __set_hyper_parameters(self):
        self.hyper_parameters.append({
            "mini_batch": self.target_params["mini-batch"],
            "epochs": self.target_params["epochs"],
            "learning_rate": float(self.learning_rate),
            "optimizer": str(self.optimizer),
            "training_set": self.target_params["n_train"],
            "validation_set": self.target_params["n_validation"],
            "test_set": self.target_params["n_test"]
        })

    def save_results_in_database(self, elapsed_time, model_name):
        sca_keras_model = ScaKerasModels()
        keras_model_description = sca_keras_model.keras_model_as_string(model_name)
        self.db_inserts = DatabaseInserts('database.sqlite', self.target_params["name"], "script_pkc.py", elapsed_time)
        self.db_inserts.save_neural_network(keras_model_description, model_name)
        self.db_inserts.save_hyper_parameters(self.hyper_parameters)

    def save_metrics(self, test_acc):
        m_index = 0
        for metric in self.metric_names:
            self.__save_metric(self.metric_training[m_index], metric)
            self.__save_metric(self.metric_validation[m_index], "val_" + metric)
            if metric == "accuracy":
                self.__save_metric(test_acc, "test_" + metric)
            m_index += 1

    def set_hyper_parameters(self):
        self.__set_hyper_parameters()

    def get_metrics_results(self, history):
        m_index = 0
        for metric in self.metric_names:
            self.metric_training[m_index] = history.history[metric]
            self.metric_validation[m_index] = history.history['val_' + metric]
            m_index += 1
