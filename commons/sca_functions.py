import numpy as np
import random
from crypto.aes import AES
from sklearn.utils import shuffle


class ScaFunctions:

    def ge_and_sr(self, runs, model, param, aes_leakage_model, x_test, y_test, step, fraction, labels_kg=None):

        nt = len(x_test)
        nt_kr = int(nt / fraction)
        nt_interval = int(nt / (step * fraction))
        key_ranking_sum = np.zeros(nt_interval)
        success_rate_sum = np.zeros(nt_interval)

        # ---------------------------------------------------------------------------------------------------------#
        # compute labels for key hypothesis
        # ---------------------------------------------------------------------------------------------------------#
        if labels_kg is None:
            crypto = AES()
            labels_key_hypothesis = np.zeros((param["number_of_key_hypothesis"], nt))
            for key_byte_hypothesis in range(0, param["number_of_key_hypothesis"]):
                key_h = bytearray.fromhex(param["key"])
                key_h[aes_leakage_model["byte"]] = key_byte_hypothesis
                crypto.set_key(key_h)
                labels_key_hypothesis[key_byte_hypothesis][:] = crypto.aes_labelize(y_test,
                                                                                    aes_leakage_model,
                                                                                    param,
                                                                                    key_hypothesis=True)
        else:
            labels_key_hypothesis = labels_kg

        # ---------------------------------------------------------------------------------------------------------#
        # predict output probabilities for shuffled test or validation set
        # ---------------------------------------------------------------------------------------------------------#
        output_probabilities = model.predict(x_test)

        probabilities_kg_all_traces = np.zeros((nt, param["number_of_key_hypothesis"]))
        for index in range(nt):
            probabilities_kg_all_traces[index] = output_probabilities[index][
                np.asarray([int(leakage[index]) for leakage in labels_key_hypothesis[:]])
            ]

        for run in range(runs):

            probabilities_kg_all_traces_shuffled = shuffle(probabilities_kg_all_traces,
                                                           random_state=random.randint(0, 100000))

            key_probabilities = np.zeros(param["number_of_key_hypothesis"])

            kr_count = 0
            for index in range(nt_kr):

                key_probabilities += np.log(probabilities_kg_all_traces_shuffled[index] + 1e-36)
                key_probabilities_sorted = np.argsort(key_probabilities)[::-1]

                if (index + 1) % step == 0 and index > 0:
                    key_ranking_good_key = list(key_probabilities_sorted).index(param["good_key"]) + 1
                    key_ranking_sum[kr_count] += key_ranking_good_key

                    if key_ranking_good_key == 1:
                        success_rate_sum[kr_count] += 1

                    kr_count += 1

            print(
                "Computing Guessing Entropy - KR: {} | final GE for correct key ({}): {})".format(run, param["good_key"],
                                                                                                  key_ranking_sum[nt_interval - 1] / (
                                                                                                              run + 1)))

        guessing_entropy = key_ranking_sum / runs
        success_rate = success_rate_sum / runs

        return guessing_entropy, success_rate
