class ScaParameters:

    def __init__(self):
        self.trace_set_list = []

    def get_trace_set(self, trace_set_name):
        trace_list = self.get_trace_set_list()
        return trace_list[trace_set_name]

    def get_basic_metrics(self):
        return ["acc", "val_acc", "test_acc", "recall", "val_recall", "loss", "val_loss"]

    def get_trace_set_list(self):
        parameters_ascad_fixed_key = {
            "name": "ascad",
            "key": "00112233445566778899AABBCCDDEEFF",
            "key_offset": 16,
            "input_offset": 0,
            "data_length": 50,
            "number_of_samples": 700,
            "n_train": 49000,
            "n_validation": 1000,
            "n_test": 10000,
            "classes": 9,
            "good_key": 34,
            "number_of_key_hypothesis": 256,
            "epochs": 20,
            "mini-batch": 400,
            "random_key": True
        }

        parameters_sim_hw = {
            "name": "simulated_random_key",
            "key": "CAFEBABEDEADBEEF0001020304050607",
            "key_offset": 16,
            "input_offset": 0,
            "data_length": 32,
            "first_sample": 0,
            "number_of_samples": 250,
            "n_train": 25000,
            "n_validation": 1000,
            "n_test": 10000,
            "classes": 9,
            "good_key": 202,
            "number_of_key_hypothesis": 256,
            "epochs": 25,
            "mini-batch": 400,
            "random_key": True
        }

        parameters_ecc = {
            "name": "ecc",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 1,
            "first_sample": 0,
            "number_of_samples": 550,
            "n_train": 25500,
            "n_validation": 255,
            "n_test": 255,
            "trs_file_training": "ECC_Training + TraceNormalization.trs",
            "trs_file_test": "ECC_Test + TraceNormalization.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 40,
            "mini-batch": 64,
            "random_key": False
        }

        parameters_ecc_ha = {
            "name": "ecc_ha",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 8000,
            "n_train": 31875,
            "n_validation": 31875,
            "n_test": 12750,
            "trs_file_training": "ecc_from_ha_training.trs",
            "trs_file_test": "ecc_from_ha_test.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 10,
            "mini-batch": 400,
            "random_key": False
        }

        parameters_ecc_ha_winres = {
            "name": "ecc_ha_winres",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 800,
            "n_train": 31875,
            "n_validation": 31875,
            "n_test": 12750,
            "trs_file_training": "ecc_from_ha_training + winres + TraceNormalization.trs",
            "trs_file_test": "ecc_from_ha_test + winres + TraceNormalization.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 10,
            "mini-batch": 400,
            "random_key": False
        }

        parameters_ecc_ha_winres_no_opt = {
            "name": "ecc_ha_winres_no_opt",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 800,
            "n_train": 10200,
            "n_validation": 10200,
            "n_test": 5100,
            "trs_file_training": "ecc_from_ha_training_no_opt + winres + TraceNormalization.trs",
            "trs_file_test": "ecc_from_ha_test_no_opt + winres + TraceNormalization.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 25,
            "mini-batch": 64,
            "random_key": False
        }

        parameters_ecc_ha_no_opt = {
            "name": "ecc_ha_no_opt",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 1000,
            "number_of_samples": 3000,
            "n_train": 10200,
            "n_validation": 10200,
            "n_test": 5100,
            "trs_file_training": "ecc_from_ha_training_no_opt + winres + TraceNormalization.trs",
            "trs_file_test": "ecc_from_ha_test_no_opt + winres + TraceNormalization.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 25,
            "mini-batch": 64,
            "random_key": False
        }

        parameters_ecc_ha_winres_pointer = {
            "name": "ecc_ha_winres_pointer",
            "key": "4DFBE0F27221FE10A78D4ADC8E490469",
            "key_offset": 0,
            "input_offset": 0,
            "data_length": 2,
            "first_sample": 0,
            "number_of_samples": 1000,
            "n_train": 10200,
            "n_validation": 10200,
            "n_test": 5100,
            "trs_file_training": "ecc_from_ha_training_no_opt_pointer + TraceNormalization.trs",
            "trs_file_test": "ecc_from_ha_test_no_opt_pointer + TraceNormalization.trs",
            "classes": 2,
            "good_key": 0,
            "epochs": 25,
            "mini-batch": 64,
            "random_key": False
        }

        self.trace_set_list = {
            "ascad": parameters_ascad_fixed_key,
            "simulated_random_key": parameters_sim_hw,
            # add more here,
            # PKC
            "ecc": parameters_ecc,
            "ecc_ha": parameters_ecc_ha,
            "ecc_ha_winres": parameters_ecc_ha_winres,
            "ecc_ha_winres_no_opt": parameters_ecc_ha_winres_no_opt,
            "ecc_ha_no_opt": parameters_ecc_ha_no_opt,
            "ecc_ha_winres_pointer": parameters_ecc_ha_winres_pointer
        }

        return self.trace_set_list
