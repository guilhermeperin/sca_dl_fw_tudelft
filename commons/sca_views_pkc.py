from commons.sca_database import ScaDatabase
from commons.sca_parameters import ScaParameters
from database.tables import KeyRankJSON, SuccessRateJSON, Metric, AccuracyJSON, AccuracyIterationJSON
from plots.PlotlyPlots import PlotlyPlots


class ScaViewsPKC:

    def __init__(self, analysis_id):
        self.db = ScaDatabase('scripts_pkc/database.sqlite')
        self.analysis_id = analysis_id

    def accuracy_pkc_plots(self):

        plotly_plots = PlotlyPlots()

        all_accuracy_set1_plots = []
        all_accuracy_set2_plots = []
        accuracy_pkc_plots = []

        all_accuracy_values = self.db.select_values_from_accuracy_json(AccuracyJSON, self.analysis_id)
        for all_accuracy_value in all_accuracy_values:
            if "acc_dl_set1_" in all_accuracy_value['name']:
                all_accuracy_set1_plots.append(
                    plotly_plots.create_line_plot(y=all_accuracy_value['values'], line_name=all_accuracy_value['name']))
            if "acc_dl_set2_" in all_accuracy_value['name']:
                all_accuracy_set2_plots.append(
                    plotly_plots.create_line_plot(y=all_accuracy_value['values'], line_name=all_accuracy_value['name']))

        accuracy_pkc_plots.append({
            "title": "Accuracy Deep Learning Set1",
            "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "Accuracy DL Set1"),
            "plots": all_accuracy_set1_plots
        })

        if len(all_accuracy_set2_plots) > 0:
            accuracy_pkc_plots.append({
                "title": "Accuracy Deep Learning Set2",
                "layout_plotly": plotly_plots.get_plotly_layout("Epochs", "Accuracy DL Set2"),
                "plots": all_accuracy_set2_plots
            })

        return accuracy_pkc_plots

    def accuracy_iteration_pkc_plots(self):

        plotly_plots = PlotlyPlots()

        all_accuracy_set_plots = []
        accuracy_pkc_plots = []

        all_accuracy_values = self.db.select_values_from_accuracy_json(AccuracyIterationJSON, self.analysis_id)
        for all_accuracy_value in all_accuracy_values:
            all_accuracy_set_plots.append(
                plotly_plots.create_line_plot(y=all_accuracy_value['values'], line_name=all_accuracy_value['name']))

        accuracy_pkc_plots.append({
            "title": "Accuracy vs Iteration",
            "layout_plotly": plotly_plots.get_plotly_layout("Iteration", "Accuracy"),
            "plots": all_accuracy_set_plots
        })

        return accuracy_pkc_plots
