import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime, ForeignKey, JSON, Boolean
from sqlalchemy.orm import relationship

Base = declarative_base()


def base():
    return Base


class Analysis(Base):
    __tablename__ = 'analysis'
    id = Column(Integer, primary_key=True)
    datetime = Column(DateTime, default=datetime.datetime.utcnow)
    script = Column(String)
    db_filename = Column(String)
    dataset = Column(String)
    elapsed_time = Column(Float)

    def __repr__(self):
        return "<Analysis(datetime=%s, script='%s')>" % (self.datetime, self.script)


class HyperParameter(Base):
    __tablename__ = 'hyper_parameter'
    id = Column(Integer, primary_key=True)
    hyper_parameters = Column(JSON)
    analysis_id = Column(Integer, ForeignKey('analysis.id'))
    analysis = relationship("Analysis")

    def __repr__(self):
        return "<HyperParemeters(id=%d)>" % self.id


class NeuralNetwork(Base):
    __tablename__ = 'neural_network'
    id = Column(Integer, primary_key=True)
    model_name = Column(String)
    description = Column(String)
    analysis_id = Column(Integer, ForeignKey('analysis.id'))
    analysis = relationship("Analysis")

    def __repr__(self):
        return "<NeuralNetwork(name=%s, description='%s')>" % (self.model_name, self.description)


class LeakageModel(Base):
    __tablename__ = 'leakage_model'
    id = Column(Integer, primary_key=True)
    leakage_model = Column(JSON)
    analysis_id = Column(Integer, ForeignKey('analysis.id'))
    analysis = relationship("Analysis")

    def __repr__(self):
        return "<LeakageModel(id=%d)>" % self.id


class Metric(Base):
    __tablename__ = 'metric'
    id = Column(Integer, primary_key=True)
    value = Column(Float)
    key_byte = Column(Integer)
    metric = Column(String)
    analysis_id = Column(Integer, ForeignKey('analysis.id'))
    analysis = relationship("Analysis")

    def __repr__(self):
        return "<Metric(value=%f, metric='%s')>" % (self.value, self.metric)


class KeyRankJSON(Base):
    __tablename__ = 'key_rank_json'
    id = Column(Integer, primary_key=True)
    values = Column(JSON)
    name = Column(String)
    key_byte = Column(Integer)
    analysis_id = Column(Integer, ForeignKey('analysis.id'))
    analysis = relationship("Analysis")

    def __repr__(self):
        return "<KeyRank(key_byte=%d)>" % self.key_byte


class SuccessRateJSON(Base):
    __tablename__ = 'success_rate_json'
    id = Column(Integer, primary_key=True)
    values = Column(JSON)
    key_byte = Column(Integer)
    analysis_id = Column(Integer, ForeignKey('analysis.id'))
    analysis = relationship("Analysis")

    def __repr__(self):
        return "<SuccessRate(key_byte=%d)>" % self.key_byte


class AccuracyJSON(Base):
    __tablename__ = 'accuracy_json'
    id = Column(Integer, primary_key=True)
    values = Column(JSON)
    name = Column(String)
    key_byte = Column(Integer)
    analysis_id = Column(Integer, ForeignKey('analysis.id'))
    analysis = relationship("Analysis")

    def __repr__(self):
        return "<AccuracyJson(key_byte=%d)>" % self.key_byte


class AccuracyIterationJSON(Base):
    __tablename__ = 'accuracy_iteration_json'
    id = Column(Integer, primary_key=True)
    values = Column(JSON)
    name = Column(String)
    key_byte = Column(Integer)
    analysis_id = Column(Integer, ForeignKey('analysis.id'))
    analysis = relationship("Analysis")

    def __repr__(self):
        return "<AccuracyIterationJSON(key_byte=%d)>" % self.key_byte
