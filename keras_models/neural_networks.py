from keras import backend as backend
from keras.optimizers import Adam, RMSprop, SGD
from keras.layers import Flatten, Dense, Conv1D, MaxPooling1D, BatchNormalization, AveragePooling1D, Input
from keras import Sequential, Model
import random


class NeuralNetwork:

    def recall(self, classes):
        def recall(y_true, y_pred):
            class_acc = 0
            for class_id in range(classes):
                class_id_true = backend.argmax(y_true, axis=-1)
                class_id_preds = backend.argmax(y_pred, axis=-1)
                # Replace class_id_preds with class_id_true for recall here
                accuracy_mask = backend.cast(backend.equal(class_id_true, class_id), 'int32')
                class_acc_tensor = backend.cast(backend.equal(class_id_true, class_id_preds), 'int32') * accuracy_mask
                class_acc += backend.sum(class_acc_tensor) / backend.maximum(backend.sum(accuracy_mask), 1)
            return class_acc / classes

        return recall

    def cnn(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=16, kernel_size=10, strides=10, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Flatten())
        model.add(Dense(200, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(200, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(200, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.00088)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp(self, classes, number_of_samples):
        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples,)))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(600, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = Adam(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_ecc_ha(self, classes, number_of_samples):
        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples,)))
        model.add(Dense(800, activation='relu'))
        model.add(Dense(400, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(100, activation='relu'))
        model.add(Dense(50, activation='relu'))
        model.add(Dense(100, activation='relu'))
        model.add(Dense(200, activation='relu'))
        model.add(Dense(400, activation='relu'))
        model.add(Dense(800, activation='relu'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer,
                      metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_small(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=8, kernel_size=20, strides=4, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=20, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=20, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha(self, classes, number_of_samples):
        model = Sequential()
        model.add(Conv1D(filters=8, kernel_size=40, strides=4, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_ecc_ha_big(self, classes, number_of_samples):
        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=16, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=32, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=64, kernel_size=40, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(100, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        optimizer = RMSprop(lr=0.0000001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def mlp_random(self, classes, number_of_samples):

        activation_function = ['relu', 'tanh', 'selu', 'elu', 'sigmoid'][random.randint(0, 2)]
        neurons = random.randint(50, 200)
        layers = random.randint(1, 5)

        model = Sequential()
        model.add(BatchNormalization(input_shape=(number_of_samples,)))
        for l_i in range(layers):
            model.add(Dense(neurons, activation=activation_function))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        # optimizer = Adam(lr=0.001)
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model

    def cnn_random(self, classes, number_of_samples):

        activation_function = ['relu', 'tanh', 'selu', 'elu', 'sigmoid'][random.randint(0, 2)]
        neurons = random.randint(100, 400)
        layers = random.randint(1, 5)

        model = Sequential()
        model.add(Conv1D(filters=16, kernel_size=4, strides=4, activation='relu', padding='valid',
                         input_shape=(number_of_samples, 1)))
        model.add(Conv1D(filters=32, kernel_size=4, strides=4, activation='relu', padding='valid'))
        model.add(Conv1D(filters=64, kernel_size=4, strides=4, activation='relu', padding='valid'))
        model.add(Flatten())
        for l_i in range(layers):
            model.add(Dense(neurons, activation=activation_function))
        model.add(Dense(classes, activation='softmax'))
        model.summary()
        # optimizer = Adam(lr=0.001)
        optimizer = RMSprop(lr=0.0001)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy', self.recall(classes)])

        return model
