import plotly
import plotly.graph_objs as go
import pandas as pd
import numpy as np
import json


class PlotlyPlots:

    def create_line_plot(self, x=None, y=None, line_name="line_plot"):
        if x is None:
            x = np.linspace(1, len(y), len(y))

        df = pd.DataFrame({'x': x, 'y': y})  # creating a sample dataframe
        data = [
            go.Line(
                x=df['x'],  # assign x as the dataframe column 'x'
                y=df['y'],
                name=line_name,
                line={
                    'width': 1.5
                }
            )
        ]
        return json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)

    def get_plotly_layout(self, x_axis_title, y_axis_title):
        layout = {
            "annotations": [],
            "font": {
                "size": 14,
                "family": 'Calibri',
                "color": '#263238'
            },
            "xaxis": {
                "ticks": '',
                "side": 'bottom',
                "title": x_axis_title,
                "tickcolor": '#fff',
                "gridcolor": '#d0d0d0',
                "color": '#263238'
            },
            "yaxis": {
                "ticks": '',
                "ticksuffix": ' ',
                "autosize": False,
                "title": y_axis_title,
                "tickcolor": '#fff',
                "gridcolor": '#d0d0d0',
                "color": '263238'
            },
            'paper_bgcolor': '#fafafa',
            'plot_bgcolor': '#fafafa'
        }

        return layout
