from keras_models.neural_networks import NeuralNetwork
from commons.sca_deep_aes import ScaDeep

sca_deep = ScaDeep()

#  define target
sca_deep.target("simulated_random_key")

#  define leakage model (models available: HW, ID and bit)
sca_deep.aes_leakage_model(leakage_model="HW")

#  define model
sca_deep.keras_model(NeuralNetwork().mlp, mlp=True)

# ge_sr = [Number of Key Ranks, Trace Report Interval, Fraction of full test set to compute each key rank]
# example:
# Number of Test Traces = 10000
# ge_sr=[100, 100, 10]
# guessing entropy is the average of 100 key ranks
# the guessing entropy is reported for each 100 test traces (to speed up the computation process)
# each key rank is computed by randomly selecting 10000/10 = 1000 traces from full test set

# target_key_bytes=[2]: attack AES key byte of index 2

#  train/validate and test
key_ranks = sca_deep.run(
    target_key_bytes=[0],
    ge_sr=[100, 100, 10]
)
