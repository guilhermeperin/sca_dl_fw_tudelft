import sys
sys.path.append('C:\\Users\\guilh\\PycharmProjects\\dl_sca_framework')

# import sys
# sys.path.append('/home/guilherme/sca_dl_fw_tudelft')

from keras_models.neural_networks import NeuralNetwork
from commons.sca_deep_pkc import ScaDeepPKC

sca_deep = ScaDeepPKC()
sca_deep.target("ecc_ha_winres_no_opt")
sca_deep.set_mini_batch(400)
sca_deep.set_epochs(20)
sca_deep.keras_model(NeuralNetwork().cnn_ecc_ha_small, cnn=True)
sca_deep.run_recursive(20)
# sca_deep.run()

# sca_deep = ScaDeepPKC()
# sca_deep.target("ecc_ha_winres")
# sca_deep.run_cluster()
